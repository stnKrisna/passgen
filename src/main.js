(function(){
  var electron = require('electron');
  var cb = electron.clipboard;
  var fs = require('fs');
  var path = require('path');
  var os = require('os');
  var releaseVersions = os.release().split('.');
  var cookie = require('electron-json-storage');

  window.cookie = cookie;

  var passwordContainer = document.getElementById('passwordContainer');
  var settingContainer = document.getElementById('setting');

  if(!(os.platform() === 'darwin' && releaseVersions[0] >= 13)){
    document.getElementById('topBar').style.display = 'none';
    document.getElementById('content').classList.add('fullWindow');
  }

  window.fs = fs;
  window.electron = electron;
  window.path = path;

  var wordList = [];

  setAppTheme();

  // https://stackoverflow.com/a/18278346
  function loadJSON(path, success, error){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          if (success)
          success(xhr.responseText);
        } else {
          if (error)
          error(xhr);
        }
      }
    };
    xhr.open("GET", path, true);
    xhr.send();
  }

  loadJSON('words.txt',
    function(data) {
      wordList = data.split('\n');
      passwordContainer.type = 'password';
      passwordContainer.value = generatePassword(Math.randomRange(12,20, INT));
    },
    function(xhr) { throw xhr; }
  );

  var specialChars = '!@#$%^&*()_+-=[]{}\\|;:\'"~\`,<.>/?';
  Math.seed = (new Date()).getTime();
  var password = '';

  function generateBytes(count){
    var tmp = 0;
    var bytes = '';
    /* Generate random byte */
    for(var i = 0; i < count; i++){
      tmp = Math.randomSeed(0,255,INT);

      // if(Math.randomRange(0,100) >= 55){
      //   i--;
      //   password += specialChars[Math.randomRange(0, specialChars.length - 1, INT)];
      // }

      // if(Math.random() >= 0.6)
      //   password += tmp <= 15 ? '0' + tmp.toString(16).toUpperCase() : tmp.toString(16).toUpperCase();
      // else
        bytes += tmp <= 15 ? '0' + tmp.toString(16) : tmp.toString(16);
    }

    return bytes;
  }

  function generatePassword(trimTo){
    password = '';
    var bytes = '', bytesSplit = ['', ''], bytesSplitMarker;
    var prefix, suffix;
    var randomWord = [], randomUpperCase = 0;
    var cutAt, passCut = ['',''];

    /* Pick 2 random special char */
    prefix = Math.randomSeed(0, specialChars.length - 1, INT);

    do{
      suffix = Math.randomSeed(0, specialChars.length - 1, INT);
    }while (suffix == prefix);

    /* Generate random bytes for the initial password */
    bytes = generateBytes(5);
    bytesSplitMarker = Number.parseInt(bytes.length / 2);

    bytesSplit[0] = bytes.slice(0, bytesSplitMarker);
    bytesSplit[1] = bytes.slice(bytesSplitMarker, -1).toUpperCase();

    password = bytesSplit[0] + bytesSplit[1];

    /* Add prefix special character */
    password = specialChars[prefix] + specialChars[suffix] + password;

    /* Add random word from dictionary */
    cutAt = Math.randomSeed(5, ((specialChars.length - 1) / Math.randomRange(2,5,INT)) + 5, INT);
    cutAt = cutAt >= password.length ? 5 : cutAt;

    passCut[0] = password.slice(0, cutAt);
    passCut[1] = password.slice(cutAt, -1);

    randomWord = wordList[Math.randomRange(0, wordList.length - 1, INT)].split('');

    randomUpperCase = Math.randomRange(0, randomWord.length - 1, INT); // Convert random char to upper case
    randomWord[randomUpperCase] = randomWord[randomUpperCase].toUpperCase();

    // Add random special char to random string
    for(var i = 0; i < Math.randomRange(3, 5, INT); i++){
      randomWord[Math.randomRange(0, randomWord.length - 1, INT)] += specialChars[Math.randomSeed(0, specialChars.length - 1, INT)];
    }

    password = passCut[0] + randomWord.join('') + passCut[1];

    /* Trim password */
    if(trimTo > 1)
    password = password.slice(0, trimTo);

    // console.log('Password: ' + password);

    return password;
  }

  function setAppTheme(themeName){

    if(themeName == undefined){
      cookie.get('theme', function(e, c){
        cookie.get('theme', function(e, c){
          console.log(e, c);
        });

        var theme = c;

        if(c === null){
          cookie.set('theme', {value: 'light'}, function(e){console.log(e)});
          switchTheme('light');
        }else{
          switchTheme(c.value);
        }
      });

      return;
    }else{
      cookie.set('theme', {value: themeName}, function(e){console.log(e)}, function(e){console.log(e)});
      switchTheme(themeName);
    }


  }

  function switchTheme(themeName){
    var elements = document.getElementsByClassName('hasTheme');
    var theme = (themeName == undefined ? 'light' : themeName);

    for(var i = 0; i < elements.length; i++){
      if(theme === 'light'){
        elements[i].classList.remove('dark');
      }else if(theme === 'dark'){
        elements[i].classList.add('dark');
      }
    }
  }

  /* Generate Bytes */
  document.getElementById('generateBytes').addEventListener('click', function(){
    passwordContainer.value = generateBytes(8);
    passwordContainer.type = 'text';
  });

  /* Generate password */
  document.getElementById('generatePassword').addEventListener('click', function(){
    passwordContainer.type = 'password';
    passwordContainer.value = generatePassword();
  });

  /* Copy to clipboard */
  document.getElementById('toClipboard').addEventListener('click', function(){
    cb.writeText(password);
  });

  /* Show password */
  document.getElementById('showPassword').addEventListener('mousedown', function(){
    passwordContainer.type = 'text';
    this.innerHTML = 'Hide Password';
  });

  document.getElementById('showPassword').addEventListener('mouseup', function(){
    passwordContainer.type = 'password';
    this.innerHTML = 'Show Password';
  });

  /* Toggle setting */
  document.getElementById('settingButton').addEventListener('click', function(){
    settingContainer.classList.add('show');
  });

  document.getElementById('settingClose').addEventListener('click', function(){
    settingContainer.classList.remove('show');
  });

  /* Theme switcher */
  document.getElementById('themeLight').addEventListener('click', function(){
    setAppTheme('light');
  });
  document.getElementById('themeDark').addEventListener('click', function(){
    setAppTheme('dark');
  });

  window.settingContainer = settingContainer;
  window.generatePassword = generatePassword;
}())
