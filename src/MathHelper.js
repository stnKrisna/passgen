function createVector(x, y, z){
  var _x = x, _y = y, _z = z;

  if(x instanceof Array){
    _x = x[0] || 0;
    _y = x[1] || 0;
    _z = x[2] || 0;
  }

  if(x instanceof Vector3){
    _x = x.x || 0;
    _y = x.y || 0;
    _z = x.z || 0;
  }

  return new Vector3(_x, _y, _z);
}

class Vector3{
  constructor(x,y,z){
    this.x = x;
    this.y = y;
    this.z = z;
  }
};

/* Vector3 constants */
Vector3.MANHATTAN = 0;
Vector3.CHEBYSHEV = 1;
Vector3.EUCLIDEAN = 2;

Vector3.LERP = 3;
Vector3.SLERP = 4;
Vector3.N_LERP = 5;

/* Vector3 functions */
Vector3.dot = function (x, y, z) {
  var a, b, v;

  /* Convert input to vector */
  if(x instanceof Vector3 && y instanceof Vector3){ // 2 vector object was given (will only work when called from Vector3 class)
    a = createVector(x.x, x.y, x.z);
    b = createVector(y.x, y.y, y.z);

    return a.x * (b.x)+
           a.y * (b.y)+
           a.z * (b.z);
  }else{ // 1 vector object or non was given
    v = createVector(x, y, z);
  }

  return this.x * (v.x)+
         this.y * (v.y)+
         this.z * (v.z);
};

Vector3.cross = function(x, y, z){
  var a, b, v;
  var _x, _y, _z;

  /* Convert input to vector */
  if(x instanceof Vector3 && y instanceof Vector3){ // 2 vector object was given (will only work when called from Vector3 class)
    a = createVector(x.x, x.y, x.z);
    b = createVector(y.x, y.y, y.z);

    _x = a.y * b.z - a.z * b.y;
    _y = a.z * b.x - a.x * b.z;
    _z = a.x * b.y - a.y * b.x;

    return new Vector3(_x, _y, _z);
  }else{ // 1 vector object or non was given
    v = createVector(x, y, z);
  }

  return Vector3.cross(this, v);
}

Vector3.prototype.dot = Vector3.dot;
Vector3.prototype.cross = Vector3.cross;

Vector3.prototype.limit = function (max) {
  var m = this.magnitudeSq();

  if(m > (max * max)){
    this.div(Math.sqrt(m));
    this.mult(max);
  }

  return this;
};

Vector3.prototype.setMagnitude = function (n) {
  return this.normalized().mult(n);
};

Vector3.prototype.equals = function (x, y, z) {
  var v = createVector(x, y, z);
  return (this.x === v.x) && (this.y === v.y) && (this.z === v.z);
};

Vector3.prototype.dist = function (method, x, y, z) {
  var a = createVector(this.x, this.y, this.z);
  var b = createVector(x, y, z);

  switch (method) {
    case Vector3.MANHATTAN: return (Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z));

    case Vector3.CHEBYSHEV: return Math.max(Math.abs(a.x - b.x), Math.abs(a.y - b.y), Math.abs(a.z - b.z));

    case Vector3.EUCLIDEAN: return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) + Math.pow(a.z - b.z, 2));
    default:

  }
};

Vector3.prototype.copy = function () {
  return new Vector3(this.x, this.y, this.z);
};

Vector3.prototype.add = function (x, y, z) {
  var v = createVector(x, y, z);

  this.x += v.x || 0;
  this.y += v.y || 0;
  this.z += v.z || 0;

  return this;
};

Vector3.prototype.sub = function (x, y, z) {
  var v = createVector(x, y, z);

  this.x -= v.x || 0;
  this.y -= v.y || 0;
  this.z -= v.z || 0;

  return this;
};

Vector3.prototype.magnitude = function () {
  return Math.sqrt(this.magnitudeSq());
};

Vector3.prototype.magnitudeSq = function () {
  var x = this.x, y = this.y, z = this.z;
  return (x * x + y * y + z * z);
};

Vector3.prototype.div = function (n) {
  this.x /= n;
  this.y /= n;
  this.z /= n;
  return this;
};

Vector3.prototype.mult = function (n) {
  this.x *= n || 0;
  this.y *= n || 0;
  this.z *= n || 0;
  return this;
};

Vector3.prototype.normalized = function () {
  return this.magnitude() === 0 ? this : this.div(this.magnitude());
};

Vector3.prototype.array = function () {
  return [this.x, this.y, this.z];
};

Vector3.prototype.heading = function () { // Calculate heading between 2 vector (2D only)
  var h = Math.atan2(this.y, this.x);
  return h; // In radian
};

Vector3.prototype.rotate = function (rotateVector, radian) {
  // http://www.nh.cas.cz/people/lazar/celler/online_tools.php
  var v1, v2; // Cached vector
  var v1_f; // Final vector
  var m1, m2; // Vector's magnitude
  var c, s, t; // Cos, sin, theta

  /* Copy vectors */
  v1 = this.copy();
  v2 = rotateVector.copy();

  /* Get magnitude of vectors */
  m1 = v1.magnitude();
  m2 = v2.magnitude();

  /* Normalize vectors */
  v1.normalized();
  v2.normalized();

  /* Trig stuff */
  c = Math.cos(radian);
  s = Math.sin(radian);
  t = 1 - c;

  v1_f = new Vector3(
    v1.x * (t * (v2.x * v2.x) + c) + v1.y * (t * (v2.x * v2.y) - s * v2.z) + v1.z * (t * (v2.x * v2.z) + s * v2.y),
    v1.x * (t * (v2.x * v2.y) + s * v2.z) + v1.y * (t * (v2.y * v2.y) + c) + v1.z * (t * (v2.y * v2.z) - s * v2.x),
    v1.x * (t * (v2.x * v2.z) - s * v2.y) + v1.y * (t * (v2.y * v2.z) + s * v2.x) + v1.z * (t * (v2.z * v2.z ) + c)
  );

  v1_f.mult(m1);

  v1_f.x = Math.round(v1_f.x*1000000)/1000000;
  v1_f.y = Math.round(v1_f.y*1000000)/1000000;
  v1_f.z = Math.round(v1_f.z*1000000)/1000000;

  return v1_f;
};

/* Vector interpolation */
Vector3.interpolate = function (start, end, amount, method) {
  var v1, v2;

  v1 = start.copy();
  v2 = end.copy();
  switch (method) {
    case Vector3.LERP:
      return (v1.add(v2.sub(v1).mult(amount)));
    // case Vector3.SLERP: // Currently broken...
    //   /* Normalize start and end vector */
    //   v1.normalized();
    //   v2.normalized();
    //
    //   /* Calculate dot product */
    //   var d = v1.dot(v2);
    //
    //
    //   return (v1.mult(Math.cos(t)).add(v2.mult(Math.sin(t))));
    case Vector3.SLERP:
      throw 'exception: Method unimplemented';
      break;
    case Vector3.N_LERP:
      return Vector3.interpolate(v1, v2, amount, Vector3.LERP).normalized();
      break;
    default:

  }
};

class Dimension{
  constructor(w,h){
    this.w = w;
    this.h = h;
  }
};

Dimension.prototype.area = function () {
  return this.w * this.h;
};

Dimension.prototype.perimeter = function () {
  return (this.w * 2) + (this.h * 2);
};

Dimension.prototype.array = function () {
  return [this.w, this.h];
};

class Color{
  constructor(r,g,b){
    this.r = r;
    this.g = g;
    this.b = b;
  }
};

/* Seeded random */
//http://indiegamr.com/generate-repeatable-random-numbers-in-js/

// the initial seed
Math.seed = Math.random();

// in order to work 'Math.seed' must NOT be undefined,
// so in any case, you HAVE to provide a Math.seed
Math.randomSeed = function(min, max, convertTo) {
    max = max || 1;
    min = min || 0;
    convertTo = typeof convertTo === 'number' ? convertTo : FLOAT;

    Math.seed = (Math.seed * 9301 + 49297) % 233280;
    var rnd = Math.seed / 233280;
    rnd = min + rnd * (max - min);

    return (convertTo === INT ? Math.floor(rnd) : rnd);
}

/* Random within range */
Math.randomRange = function(min, max, convertTo){
  max = max || 1;
  min = min || 0;
  convertTo = typeof convertTo === 'number' ? convertTo : FLOAT;

  var rnd = min + Math.random() * (max - min);

  return (convertTo === INT ? Math.floor(rnd) : rnd);
}

/* Clamp */
Math.clamp = function(value, min, max) {
  return Math.min(Math.max(value, min), max);
};

Math.toDegree = function(r){
  return r * (180 / Math.PI);
}

Math.toRadian = function(d){
  return d * (Math.PI / 180);
}

function setGlobal(){
  var glob = (typeof window === 'undefined' ? global : window);

  /* Expose new data type to global */
  glob.createVector = createVector;
  glob.Vector3 = Vector3;
  glob.Dimension = Dimension;
  glob.Color = Color;

  /* Expose numerical data type constant to global */
  glob.INT = 0;
  glob.FLOAT = 1;
}

setGlobal();
