let mainWindow;

const APP_NAME = 'passGen.';
const process = require('process');
const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');

/* Set process name */
// process.title = APP_NAME;
app.setName(APP_NAME);

function createWindow () {
  /* Check OS release */
  var os = require('os');
  os.platform(); // 'darwin'
  os.release(); //'10.8.0'
  var releaseVersions = os.release().split('.');

  var useFrame = (os.platform() === 'darwin' && releaseVersions[0] >= 13) ? 'hidden' : 'default';

  // Enable precise memory info
  app.commandLine.appendSwitch('--enable-precise-memory-info');

  // Create the browser window.
  // mainWindow = new BrowserWindow({width: userPref.windowSize.w, height: userPref.windowSize.h, fullscreen: userPref.fullscreen, frame: userPref.frame, 'web-preferences': {'plugins': true}})
  mainWindow = new BrowserWindow({width: 500, height: 400, title: APP_NAME, backgroundColor: '#65737e', titleBarStyle: useFrame, 'web-preferences': {'plugins': true}})

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  mainWindow.setMenu(null);

  // w.send('Game window oppened');
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin' || true) {
    app.quit();
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
});
